LATEX=xelatex
PANDOCFLAGS=-s --mathjax --latex-engine=lualatex
LATEXFLAGS?=-halt-on-error -file-line-error
BIBFLAGS?=--fixinits
LATEXINDENT=latexindent
LATEXINDENT_SETTIGNS=indent.yaml

ROOT=main.tex
DIRS=$(shell find . -maxdepth 1 -type d)
SRC=$(foreach dir, $(DIRS), $(wildcard $(dir)/*.tex))

pdf: $(SRC)
	$(LATEX) $(LATEXFLAGS) $(ROOT)

final:
	$(MAKE) -C . pdf
	$(MAKE) -C . pdf
	$(MAKE) -C . biber
	$(MAKE) -C . pdf

biber:
	biber $(BIBFLAGS) $(patsubst %.tex, %.bcf, $(ROOT))

bibercheck:
	biber --validate-control $(patsubst %.tex, %.bcf, $(ROOT))

html:
	pandoc $(PANDOCFLAGS) --from latex --to html5  -o $(patsubst %.tex, %.html, $(ROOT)) $(ROOT)

doc:
	pandoc $(PANDOCFLAGS) --from latex --to odt  -o $(patsubst %.tex, %.odt, $(ROOT)) $(ROOT)
	pandoc $(PANDOCFLAGS) --from latex --to docx  -o $(patsubst %.tex, %.docx, $(ROOT)) $(ROOT)

clear:
	$(foreach dir, $(DIRS),$(foreach ext,\
*.aux *.log *.out *.xml *.blg *.bcf *.bbl *.toc *.bak* *~,\
rm -f $(dir)/$(ext) ;))



clearall: clear
	$(foreach dir, $(DIRS),$(foreach ext,\
*.pdf *.html *.odt *.doc *.docx, rm -f $(dir)/$(ext) ;))



indent:
	$(foreach name, $(SRC),\
	$(LATEXINDENT) -l=$(LATEXINDENT_SETTIGNS) -s -w $(name);)

.PHONY: pdf final biber bibercheck html doc clear clearall indent
